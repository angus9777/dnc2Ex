'use strict';

var myApp = angular.module('myApp');

myApp.service('AuthService', function($q, $http){
    var self = this;
    self.profile = null;
    self.isAuthenticated = false;

    function request(url, username_or_token, password) {
        return {
            method: 'GET',
            url: url,
            headers: {
                Accept: 'application/json',
                Authorization: 'Basic ' + btoa(username_or_token + ':' + password)
            }
        };
    }

    self.signin = function (options, successCallback, errorCallback) {
        var req = request('/auth/login', options.username, options.password);
        $http(req).
        then(function(response) {
                var token = response.data.token;
                //var uid = response.data.uid;
                if (token) {
                    self.isAuthenticated = true;
                    self.profile = response.data.profile;
                    console.log(self.profile);
                    successCallback(token);
                }

            }, function(error) {
                console.log(error.message);
                errorCallback();
            }
        );

    };

    self.signout = function () {
        self.isAuthenticated = false;
        self.profile = null;
    };

    self.authenticate = function (token) {
        var req = request('/auth/login_with_token', token, '');
        var deferred = $q.defer();
        $http(req)
            .then(function (response) {
                self.profile = response.data.profile;
                self.isAuthenticated = true;
                deferred.resolve();
            }, function () {
                self.isAuthenticated = false;
                deferred.reject();
            });
        return deferred.promise;
    };

    self.getBalance = function (params, successCallback, errorCallback) {
        //var req = request('/dnc/get_balance');
        var req = request('/dnc/get_balance', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['result']['balance']);
                    successCallback(resp['result']['balance']);
                }

            }, function(error) {
            }
        );
    }

    self.getExBalance = function (params, successCallback, errorCallback) {
        var req = request('/dnc/get_ex_balance', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['result']['balance']);
                    successCallback(resp['result']['balance']);
                }

            }, function(error) {
            }
        );
    }

    self.getPaymentId = function (successCallback, errorCallback) {
        var req = request('/dnc/get_payment_id', 'GET');
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['payment_id']);
                    successCallback(resp['payment_id']);
                }

            }, function(error) {
            }
        );
    }

    self.getExLog = function (params, successCallback, errorCallback) {
        var req = request('/dnc/get_exchange_log', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp);
                }

            }, function(error) {
            }
        );
    }

    self.inPut = function (params, successCallback, errorCallback) {
        var req = request('/dnc/input', 'PUT', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp['result']['tx_hash']);
                }
            }, function(error) {
            }
        );
    }
    self.outPut = function (params, successCallback, errorCallback) {
        var req = request('/dnc/output', 'PUT', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp['result']['tx_hash']);
                }
            }, function(error) {
            }
        );
    }
});

myApp.service('DncService', function($q, $http){
    var self = this;

    function request(url, method='GET', params='', data='') {
        return {
            method: method,
            url: url,
            headers: {
                Accept: 'application/json',
            },
            params: params,
            data: data
        };
    }

    self.getBalance = function (params, successCallback, errorCallback) {
        //var req = request('/dnc/get_balance');
        var req = request('/dnc/get_balance', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['result']['balance']);
                    successCallback(resp['result']['balance']);
                }

            }, function(error) {
            }
        );
    }

    self.getExBalance = function (params, successCallback, errorCallback) {
        var req = request('/dnc/get_ex_balance', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['result']['balance']);
                    successCallback(resp['result']['balance']);
                }

            }, function(error) {
            }
        );
    }

    self.getPaymentId = function (successCallback, errorCallback) {
        var req = request('/dnc/get_payment_id', 'GET');
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp['payment_id']);
                    successCallback(resp['payment_id']);
                }

            }, function(error) {
            }
        );
    }

    self.getExLog = function (params, successCallback, errorCallback) {
        var req = request('/dnc/get_exchange_log', 'GET', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp);
                }

            }, function(error) {
            }
        );
    }

    self.inPut = function (params, successCallback, errorCallback) {
        var req = request('/dnc/input', 'PUT', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp['result']['tx_hash']);
                }
            }, function(error) {
            }
        );
    }
    self.outPut = function (params, successCallback, errorCallback) {
        var req = request('/dnc/output', 'PUT', params=params);
        $http(req).
        then(function(response) {
                var resp = response.data;
                if (resp) {
                    console.log(resp);
                    successCallback(resp['result']['tx_hash']);
                }
            }, function(error) {
            }
        );
    }
});
myApp.factory('tokenInvalidInterceptor', ['$q', '$location', '$rootScope', 'store',
    function ($q, $location, $rootScope, store) {
    return {
        'response': function (response) {
            if (response.status == 401) {
                if (store.get('token'))
                    $rootScope.$broadcast('unauth_token');
                else
                    $rootScope.$broadcast('unauth');
                return $q.reject();
            }
            return $q.resolve(response);
        },

        'responseError': function (rejection) {
            if (rejection.status == 401) {
                if (store.get('token'))
                    $rootScope.$broadcast('unauth_token');
                else
                    $rootScope.$broadcast('unauth');
            }
            return $q.reject(rejection);
        }
    }
}]);
