import json
import time
import requests
import hashlib
from flask import request, jsonify, abort, current_app, g, Response, make_response
from . import dnc
from errors import bad_request
from ..models import DncPayment
from .. import db

@dnc.route('/get_balance', methods=['GET'])
def get_balance():
    host = request.args.get('host')

    dnc_cmd = '/json_rpc'
    data = '''{
    	"jsonrpc": "2.0",
    	"method": "getbalance",
    	"params": "{}"
    }'''
    url = 'http://' + host + dnc_cmd
    print url

    result = {}
    r = requests.post(url, data=data)
    if r.status_code == requests.codes.ok:
        print r.text
        result = r.text
    return result


@dnc.route('/get_ex_balance', methods=['GET'])
def get_ex_balance():
    host = request.args.get('host')
    if host == None:
        host = '127.0.0.1:37710'

    dnc_cmd = '/json_rpc'
    data = '''{
    	"jsonrpc": "2.0",
    	"method": "getbalance",
    	"params": "{}"
    }'''
    url = 'http://' + host + dnc_cmd
    print url

    result = {}
    r = requests.post(url, data=data)
    if r.status_code == requests.codes.ok:
        print r.text
        result = r.text
    return result


@dnc.route('/get_payment_id', methods=['GET'])
def get_payment_id():
    cur_time = int(time.time())
    payment_id = hashlib.sha256(str(cur_time)).hexdigest()
    return jsonify({
        'payment_id': payment_id
    })


@dnc.route('/get_exchange_log', methods=['GET'])
def get_exchange_log():
    uid = request.args.get('uid')
    #user = DncPayment.query.filter_by(name=username_or_token).first()
    #payments = DncPayment.query.all()
    payments = DncPayment.query.filter_by(uid=uid)
    payment_list = []
    for payment in payments:
        payment_list.append({
            'uid': payment.uid,
            'time': payment.time,
            'address': payment.to_address,
            'amount': payment.amount,
            'payment_id': payment.payment_id,
            'tx': payment.tx
        })

    return jsonify({"payments": payment_list})

@dnc.route('/input', methods=['PUT'])
def input():
    host = request.args.get('host')
    uid = request.args.get('uid')
    amount = request.args.get('amount')
    address = request.args.get('address')
    payment_id = request.args.get('payment_id')
    print host, amount, address, payment_id

    now = int(time.time())
    timeArray = time.localtime(now)
    now_str = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)

    dnc_cmd = '/json_rpc'
    url = 'http://' + host + dnc_cmd
    print url

    data = '''
    {
        "jsonrpc":"2.0",
        "method":"transfer",
        "params":{
        "destinations":[
            {
            "amount":%s,
            "address":"%s"
            }
        ],
        "payment_id":"%s",
        "fee":100000,
        "mixin":0,
        "unlock_time":0
        }
    }''' % (amount, address, payment_id)
    print data

    result = {}
    r = requests.post(url, data=data)
    if r.status_code == requests.codes.ok:
        result = r.text

        r_json = json.loads(r.text)
        dnc = DncPayment(uid=uid, to_address=address, payment_id=payment_id, amount=amount, tx =r_json['result']['tx_hash'], time=now_str)
        db.session.add(dnc)
        db.session.commit()

    return result

@dnc.route('/output', methods=['PUT'])
def output():
    host = request.args.get('host')
    uid = request.args.get('uid')
    amount = request.args.get('amount')
    address = request.args.get('address')

    now = int(time.time())
    timeArray = time.localtime(now)
    now_str = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)

    dnc_cmd = '/json_rpc'
    url = 'http://' + host + dnc_cmd
    print url

    data = '''
    {
        "jsonrpc":"2.0",
        "method":"transfer",
        "params":{
        "destinations":[
            {
            "amount":%s,
            "address":"%s"
            }
        ],
        "payment_id":"",
        "fee":100000,
        "mixin":0,
        "unlock_time":0
        }
    }''' % (amount, address)
    print data

    result = {}
    r = requests.post(url, data=data)
    if r.status_code == requests.codes.ok:
        print r.text
        result = r.text

        r_json = json.loads(r.text)
        dnc = DncPayment(uid=uid, to_address=address, payment_id='', amount='-'+amount, tx =r_json['result']['tx_hash'], time=now_str)
        db.session.add(dnc)
        db.session.commit()
    return result
