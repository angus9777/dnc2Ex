# 模拟DNC钱包与交易所操作

## 背景
本程序是目的是模拟**钱包**与**交易所**交互，强依赖**dnsd**和**simplewallet**两个服务。
安装部署信息见：https://github.com/BankLedger/safenetspace2

## 架构
本软件采用 Flask + AngularJS来实现

## 软件依赖
    sqlite3、python2.7.x

## 运行
1、 创建virtualenv，然后安装依赖，在工程目录下

    $mkdir venv
    $virtualenv venv/
    $. venv/bin/activate
    $pip install -r requirements.txt
2、 创建数据库

    $ python manager.py create_db
3、 创建用户名和密码

    $ python manager.py shell

    >>> u = User(uid='00001', name='admin')
    >>> u.password = 'admin'
    >>> db.session.add(u)
    >>> db.session.commit()

4、 运行服务

    $ python manager.py runserver

## dnsd、simplewallet相关操作(ubuntu14.04)

1、启动dnsd服务，并绑定IP和端口

    ./dnsd --rpc-bind-ip 127.0.0.1 --rpc-bind-port=57709

2、创建两个钱包，分别给用户和交易所：

    ./simplewallet --generate-new-wallet test.dnc - password 123456
    生成的地址：
    DBaviQo7QzTcNeyXyV22jHajF3WE3YyJ5U6gN1sMiABy5BVj4HeagztaXu4NhtT1GB3niZnvhA73fMZ39PboqLXjPwdn4s4

    ./simplewallet --generate-new-wallet test2.dnc - password 123456
    生成的地址：
    D9Yrxh7DkEhhTHg1LaTrLd6rbkeXG6rgYgQxa71RwXrnJPGiDTEdzUXGxhMBzJuYVw7tMjz3Ut9tS74CrhVazEaHTHLQ8Q3

3、使用rpc模式启动simplewallet服务：

    交易所simplewallet服务:
    ./simplewallet --wallet-file test.dnc --password 123456 --rpc-bind-ip 127.0.0.1 --rpc-bind-port 37710 --daemon-address 127.0.0.1:57709

    用户simplewallet服务:
    ./simplewallet --wallet-file test2.dnc --password 123456 --rpc-bind-ip 127.0.0.1 --rpc-bind-port 37711 --daemon-address 127.0.0.1:57709

4、检测服务是否正常启动

    查看钱包余额：
    curl -X POST http://127.0.0.1:37710/json_rpc -d '{
        "jsonrpc": "2.0",
        "method": "getbalance",
        "params": {}
    }'

5、参考开发文档

    http://www.darknetspace.org/development-guide.html

## 初始值设定

    为了方便测试，程序中设定了一些参数作为初始值，可自行修改。

    用户钱包服务：127.0.0.1:37711
    用户钱包址：D9Yrxh7DkEhhTHg1LaTrLd6rbkeXG6rgYgQxa71RwXrnJPGiDTEdzUXGxhMBzJuYVw7tMjz3Ut9tS74CrhVazEaHTHLQ8Q3

    交易所钱包服务：127.0.0.1:37710
    服务器钱包地址：DBaviQo7QzTcNeyXyV22jHajF3WE3YyJ5U6gN1sMiABy5BVj4HeagztaXu4NhtT1GB3niZnvhA73fMZ39PboqLXjPwdn4s4

## docker

    根据本程序及相关依赖软件，已制作成docker的image文件，可下载并导入使用
    百度网盘地址：https://pan.baidu.com/s/1gfCPpF9
    部署说明见“dnc_docker使用说明.txt”

## 友情提醒

本程序中与钱包接口调用，前端页面只提取了重要参数的录入，很多参数使用了接口的默认参数。
接口详细内容，参见[开发文档](http://www.darknetspace.org/development-guide.html)
