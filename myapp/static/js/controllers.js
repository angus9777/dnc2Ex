'use strict';

var myApp = angular.module('myApp');

myApp.controller('WelcomeCtrl', function($scope, $rootScope) {
    $scope.profile = {name: 'Stranger'};
    if ($rootScope.isLogin) {
        $scope.profile = $rootScope.profile;
    }
});

myApp.controller('LoginCtrl', function($scope, AuthService, $location, store, $rootScope) {
    if (AuthService.isAuthenticated) {
        $location.path('/');
    }

    $scope.hitMsg = '';

    function onLoginSuccess(token, uid) {
        store.set('token', token);
        //store.set('uid', uid);
        $scope.hitMsg = 'login success';
        $location.path('/');
    }

    function onLoginFailed() {
       $scope.hitMsg = 'username or password incorrect';
    }

    $scope.submit = function() {
        AuthService.signin({
            username: $scope.user.username,
            password: $scope.user.password
        }, onLoginSuccess, onLoginFailed)
    };

    $rootScope.$on('unauth_token', function () {
        store.remove('token');
        $location.path('/login');
        $scope.hitMsg = 'token expire, please login';
    });

    $rootScope.$on('unauth', function () {
        $location.path('/login');
        $scope.hitMsg = 'please login';
    });
});


myApp.controller('LogoutCtrl', function(AuthService, $location, store) {
    AuthService.signout();
    store.remove('token');
    $location.path('/');
});


myApp.controller('DncCtrl', function($scope, DncService, $location, store, $rootScope) {
    if ($rootScope.isLogin) {
        $scope.profile = $rootScope.profile;
    }
    $scope.ex_host = '127.0.0.1:37710';
    $scope.host = '127.0.0.1:37711';
    $scope.payment_id = '';
    $scope.in_amount = 0.000;
    $scope.out_amount = 0.000;
    $scope.ex_address = 'DBaviQo7QzTcNeyXyV22jHajF3WE3YyJ5U6gN1sMiABy5BVj4HeagztaXu4NhtT1GB3niZnvhA73fMZ39PboqLXjPwdn4s4';
    $scope.user_address = 'D9Yrxh7DkEhhTHg1LaTrLd6rbkeXG6rgYgQxa71RwXrnJPGiDTEdzUXGxhMBzJuYVw7tMjz3Ut9tS74CrhVazEaHTHLQ8Q3';
    $scope.out_to_address = 'D9Yrxh7DkEhhTHg1LaTrLd6rbkeXG6rgYgQxa71RwXrnJPGiDTEdzUXGxhMBzJuYVw7tMjz3Ut9tS74CrhVazEaHTHLQ8Q3';
    //$scope.input_tx_hash = '0ce278f1772a002daeb10e9d437277d0bd41bae323de6e800d10a15bf8a25e1d';
    $scope.input_tx_hash = '';

    function onFailed() {
        $scope.hitMsg = 'failed!';
    }

    function onGetBalanceSuccess(params) {
        $scope.balance = params;
    }

    function onGetExBalanceSuccess(params) {
        $scope.ex_balance = params;
    }

    function onGetExLogSuccess(params) {
        $scope.exchange_log = params['payments'];
        console.log($scope.exchange_log);
    }

    function onGetPaymentIdSuccess(params) {
        $scope.payment_id = params;
    }

    function onInPutSuccess(params) {
        $scope.input_tx_hash = params;
    }

    function onOutPutSuccess(params) {
        $scope.output_tx_hash = params;
    }

    $scope.getBalance = function() {
        var params = {'host': $scope.host};
        DncService.getBalance(params, onGetBalanceSuccess, onFailed);
    };

    $scope.getExBalance = function() {
        var params = {'host': $scope.ex_host, 'uid': $scope.profile.uid};
        if ($scope.ex_host.length<1) {
            alert("输入参数不正确! 必须输入服务器地址、payment_id和有效金额");
            return;
        }
        DncService.getBalance(params, onGetExBalanceSuccess, onFailed);
    };

    $scope.getExLog = function() {
        var params = {'host': $scope.ex_host, 'uid': $scope.profile.uid};
        if ($scope.ex_host.length < 1) {
            alert("输入参数不正确! 必须输入服务器地址和有效金额");
            return;
        }
        DncService.getExLog(params, onGetExLogSuccess, onFailed);
    };

    $scope.getPaymentId = function() {
        DncService.getPaymentId(onGetPaymentIdSuccess, onFailed);
    };

    $scope.inPut = function() {
        if ($scope.payment_id.length<64 || $scope.host.length<1 || $scope.in_amount<0.0000001) {
            alert("输入参数不正确! 必须输入服务器地址、payment_id和有效金额");
            return;
        }
        var params = {'host':$scope.host, 'uid':$scope.profile.uid, 'amount':$scope.in_amount, 'payment_id':$scope.payment_id, 'address':$scope.ex_address};
        DncService.inPut(params, onInPutSuccess, onFailed);
    };

    $scope.outPut = function() {
        if ($scope.ex_host.length<1 || $scope.out_amount<0.0000001) {
            alert("输入参数不正确! 必须输入服务器地址和有效金额");
            return;
        }
        var params = {'host':$scope.ex_host, 'uid':$scope.profile.uid, 'amount':$scope.out_amount, 'address':$scope.out_to_address};
        console.log(params);
        DncService.outPut(params, onOutPutSuccess, onFailed);
    };
});
